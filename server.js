const express = require('express');
const fs = require('fs');
const cors = require('cors');
const faker = require('faker');

var app = express();

function mockApi(max){
	var data = {
		users: []
	};

	for(let i = 0; i < max; i++){
		data.users.push({
			id: i,
			name: faker.name.findName(),
			avatar: faker.internet.avatar(),
			bio: faker.lorem.paragraphs(2)
		});
	}

	return data;
}

app.use(cors());
app.set('port', process.env.PORT || 3000);
app.use(express.static('public'));

app.get('/api/v1/cats', function(req, res){
	var json = JSON.parse(fs.readFileSync('data/data.json'));
	res.send(json);
});

app.delete('/api/v1/cats/:id', function(req, res) {
  res.write('Success');
  res.end();
});

app.get('/api/v2/', function(req, res){
	var data = mockApi(100);
	res.send(data);
});

app.listen(app.get('port'), function(){
	console.log('Express started & is listening on port' + app.get('port') + '; press Ctrl-C to terminate.');
});